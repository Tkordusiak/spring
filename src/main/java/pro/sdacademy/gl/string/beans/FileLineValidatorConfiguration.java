package pro.sdacademy.gl.string.beans;


import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Configuration
@PropertySource("classpath:application.properties")
public class FileLineValidatorConfiguration {

    @Value("#{ '${correct.words}'.split(',') }")
    @Getter
    private List<String> correctWords;
}
