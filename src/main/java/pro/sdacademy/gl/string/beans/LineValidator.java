package pro.sdacademy.gl.string.beans;

public interface LineValidator {
    boolean validate(String line);
}
