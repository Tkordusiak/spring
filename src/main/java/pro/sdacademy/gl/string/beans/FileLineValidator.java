package pro.sdacademy.gl.string.beans;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
@AllArgsConstructor
public class FileLineValidator implements LineValidator{

  private final FileLineValidatorConfiguration configuration;

    @Override
    public boolean validate(String line) {
        return this.configuration.getCorrectWords().stream().anyMatch(word ->line.contains(word));
    }
}
