package pro.sdacademy.gl.string;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pro.sdacademy.gl.string.beans.TextFileParserRunner;

public class MySpringApplication {

    public static void main(String[] args) {


        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                "pro.sdacademy.gl.string");
       TextFileParserRunner runner = context.getBean(TextFileParserRunner.class);
       runner.run();
    }
}
